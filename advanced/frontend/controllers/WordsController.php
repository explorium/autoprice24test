<?php

namespace frontend\controllers;

use Yii;
use app\models\Words;
use app\models\WordsSearch;
use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WordsController implements the CRUD actions for Words model.
 */
class WordsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Words models.
     * @return mixed
     */
    public function actionIndex()
    { 
        $searchModel = new WordsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Words model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Words model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Words();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Words model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Words model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Words model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Words the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Words::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionCount($text)
    {
        $ip = Yii::$app->request->userIP;
        $res= array();
        //$text = strtolower($text);
        $text=  mb_convert_case($text, MB_CASE_LOWER, "UTF-8");
        preg_match_all("/[a-zA-Zа-яА-Я]{3,}/su",$text,$res);
        $count = array_count_values($res[0]);
        $keys = array_keys($count);
        $values = array_values($count);
        array_multisort($values, SORT_DESC, $keys, SORT_ASC);
        $usersModel = new Users();
        $user = Users::findOne(["ip" => $ip]);
        $userId=0;
        if($user !== NULL)
            $userId = $user->id;
        else
        {    
            //$usersModel = new Users();
            $usersModel->ip = $ip;
            if(!$usersModel->save()) die("Save model error");
            $userId = $usersModel->id;
        }
        
        $line='<table align="center" class = "table table-striped table-bordered" style="width:50%">';
        for ($i = 0; $i < count($keys); $i++)
        {
            $wordsModel = new Words();
            $wordsModel->users_id = $userId;
            $wordsModel->word = $keys[$i];
            $wordsModel->count = $values[$i];
            if(!$wordsModel->save()) die("Save model error");
            $line .= "<tr><td>".$keys[$i]."</td><td>".$values[$i]."</td>";
            switch ($i)
            {
                case 0:
                    $line.="<td>***</td>";
                    break;
                case 1:
                    $line.="<td>**</td>";
                    break;
                case 2:
                    $line.="<td>*</td>";
                    break;
                default:
                    $line.="<td>-</td>";
                    break;
            }
            $line.="</tr>";
        }
        
        return $line."</table>";
    }
}
