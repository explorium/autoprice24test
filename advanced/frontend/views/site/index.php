<?php
/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">
    <div class="body-content" style="text-align:center;">
        <div class ="row" id="input_div">
            <form name="words_input" id ="words_input" style ="display:block">
                <label for="wordsta" style="display:block">Enter your text here</label>
                <textarea id="wordsta" name="words" style="width: 300px; height: 200px;margin-bottom: 20px; margin-top: 10px; "></textarea>
                <p><a class="btn btn-lg btn-success" id="submit_btn" >Submit</a></p>
            </form>
        </div>
        
        <div class ="row" id="result_div" style="text-align:center; display:none">
            <p><a class="btn btn-lg btn-success" id="submit_another_btn" >Submit another text</a></p>
            <label for="words_result_ta" style="display:block">You submitted text</label>
            <textarea disabled id="words_result_ta" name="words_result_area" style="width: 300px; height: 200px;margin-bottom: 20px; margin-top: 10px;"></textarea>
            <div class ="row" id="result_table" style="text-align:center;"></div>
        </div>
    </div>
</div>