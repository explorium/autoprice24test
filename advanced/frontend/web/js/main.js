/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$("a[id='submit_btn']").on('click', function() {
    textValue = $("[name='words']").val();
    textWithBreaks = textValue.replace(/\r\n|\r|\n/g,' ');
    $("[name='words_result_area']").val(textValue);
    $("div[id='result_div']").css("display","block");
    $("div[id='input_div']").css("display","none");
    $.post("index.php?r=words/count&text="+textWithBreaks, function( data ){
        $("div[id='result_table']").html(data);
    });
})


$("a[id='submit_another_btn']").on('click', function() {
    $("[name='words']").val("");
    $("div[id='result_div']").css("display","none");
    $("div[id='input_div']").css("display","block");
})
